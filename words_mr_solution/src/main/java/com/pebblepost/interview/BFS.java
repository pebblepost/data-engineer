package com.pebblepost.interview;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * @author Pavel Belevich
 */
public class BFS {

    private static final String SPACE = " ";

    public static class BFSMapper extends Mapper<Object, Text, Text, Text> {

        @Override
        protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            final String[] parts = value.toString().split("\\s+");
            final String word = parts[0];
            final Integer phase = Integer.valueOf(parts[1]);
            final Integer dist = Integer.valueOf(parts[2]);
            final String wordsString = parts.length >= 4 ? parts[3] : "";
            if (phase == 1) {
                context.write(new Text(word), new Text(2 + SPACE + dist + SPACE + wordsString));
                final String[] adj = wordsString.split(",");
                for (String adjWord : adj) {
                    context.write(new Text(adjWord), new Text(1 + SPACE + (dist + 1) + SPACE));
                }
            } else {
                context.write(new Text(word), new Text(phase + SPACE + dist + SPACE + wordsString));
            }
        }

    }

    public static class BFSReducer extends Reducer<Text, Text, Text, Text> {

        @Override
        protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
            final Set<String> texts = StreamSupport.stream(values.spliterator(), false).map(Text::toString).collect(Collectors.toSet());
            if (texts.size() == 1) {
                context.write(key, new Text(texts.iterator().next()));
            } else {
                final String[][] parts = texts.stream().map(t -> t.split(SPACE)).toArray(String[][]::new);
                final int maxPhase = Math.max(Integer.valueOf(parts[0][0]), Integer.valueOf(parts[1][0]));
                if (maxPhase == 2) {
                    if (2 == Integer.valueOf(parts[0][0])) {
                        context.write(key, new Text(2 + SPACE + parts[0][1] + SPACE + parts[0][2]));
                    } else {
                        context.write(key, new Text(2 + SPACE + parts[1][1] + SPACE + parts[1][2]));
                    }
                } else {
                    context.write(key, new Text(1 + SPACE + Math.max(Integer.valueOf(parts[0][1]), Integer.valueOf(parts[1][1])) + SPACE + (parts[0].length > parts[1].length ? parts[0][2] : parts[1][2])));
                }
            }
        }

    }

}

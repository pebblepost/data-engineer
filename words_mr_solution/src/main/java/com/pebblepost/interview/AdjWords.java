package com.pebblepost.interview;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * @author Pavel Belevich
 */
public class AdjWords {

    public static final String BEGIN_WORD = "begin_word";

    private static final String EMPTY = "";

    public static class AdjWordsMapper extends Mapper<Object, Text, Text, Text> {

        @Override
        protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            final String word = value.toString();
            final char[] chars = word.toCharArray();
            context.write(new Text(word), new Text(EMPTY));
            for (int i = 0; i < chars.length; i++) {
                char c = chars[i];
                for (char x = 'a'; x <= 'z'; x++) {
                    if (x != c) {
                        chars[i] = x;
                        final String derivedWord = new String(chars);
                        context.write(new Text(derivedWord), new Text(word));
                    }
                }
                chars[i] = c;
            }
        }

    }

    public static class AdjWordsReducer extends Reducer<Text, Text, Text, Text> {

        private String beginWord;

        @Override
        protected void setup(Context context) throws IOException, InterruptedException {
            beginWord = context.getConfiguration().get(BEGIN_WORD);
        }

        @Override
        protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
            final Set<String> words = StreamSupport.stream(values.spliterator(), false).map(Text::toString).collect(Collectors.toSet());
            if (words.contains(EMPTY)) {
                words.remove(EMPTY);
                final String wordsString = words.stream().collect(Collectors.joining(","));
                int dist = -1;
                int phase = 0;
                if (Objects.equals(key.toString(), beginWord)) {
                    dist = 1;
                    phase = 1;
                }
                context.write(key, new Text(phase + " " + dist + " " + wordsString));
            }
        }

    }

}

package com.pebblepost.interview;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MainTest extends AbstractMiniDFSClusterTest {

    private static final String IN_DIR = "testing/words/input";
    private static final String OUT_DIR_1 = "testing/words/output1";
    private static final String OUT_DIR_2 = "testing/words/output2";
    private static final String DATA_FILE = "sample.txt";

    /**
     * 0) Read WordCount.java and WordCountTest.java to understand how to use testing environment
     * 1) Save the wordList to the HDFS
     * 2) Run your hadoop jobs
     * 3) Read the result from the HDFS
     * 4) Return the result
     *
     * @param beginWord the starting word e.g. "hit"
     * @param endWord   the ending word e.g. "cog"
     * @param wordList  the dictionary of all allowed words e.g. ["hot","dot","dog","lot","log","cog"]
     * @return the length of one shortest transformation e.g. "hit" -> "hot" -> "dot" -> "dog" -> "cog"
     */
    int words(String beginWord, String endWord, List<String> wordList) throws Throwable {
        Path inDir = new Path(IN_DIR);
        Path outDir1 = new Path(OUT_DIR_1);
        Path outDir2 = new Path(OUT_DIR_2);

        fs.delete(inDir, true);
        fs.delete(outDir1, true);

        writeHDFSContent(fs, inDir, DATA_FILE, wordList);
        conf.set(AdjWords.BEGIN_WORD, beginWord);

        Job job = Job.getInstance(conf, AdjWords.class.getName());
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        job.setMapperClass(AdjWords.AdjWordsMapper.class);
        job.setReducerClass(AdjWords.AdjWordsReducer.class);
        FileInputFormat.addInputPath(job, inDir);
        FileOutputFormat.setOutputPath(job, outDir1);
        job.waitForCompletion(true);
        assertTrue(job.isSuccessful());

        List<String[]> graph = getJobResults(fs, outDir1);
        Map<String, String[]> graphMap = graph.stream().collect(Collectors.toMap(r -> r[0], r -> r));

        assertEquals(wordList.size(), graph.size());
        for (Map.Entry<String, String[]> entry : graphMap.entrySet()) {
            final String word = entry.getKey();
            if (beginWord.equals(word)) {
                assertEquals("1", entry.getValue()[1]);
                assertEquals("1", entry.getValue()[2]);
            } else {
                assertEquals("0", entry.getValue()[1]);
                assertEquals("-1", entry.getValue()[2]);
            }
        }

        while (true) {
            long inQueue = graph.stream().filter(r -> r[1].equals("1")).count();

            if (inQueue == 0) {
                return 0;
            }

            fs.delete(outDir2, true);

            job = Job.getInstance(conf, BFS.class.getName());
            job.setOutputKeyClass(Text.class);
            job.setOutputValueClass(Text.class);
            job.setMapperClass(BFS.BFSMapper.class);
            job.setReducerClass(BFS.BFSReducer.class);
            FileInputFormat.addInputPath(job, outDir1);
            FileOutputFormat.setOutputPath(job, outDir2);
            job.waitForCompletion(true);
            assertTrue(job.isSuccessful());

            graph = getJobResults(fs, outDir2);

            Optional<Integer> result = graph.stream()
                    .filter(r -> r[0].equals(endWord) && r[1].equals("1"))
                    .map(r -> Integer.valueOf(r[2])).findFirst();

            if (result.isPresent()) {
                return result.get();
            }

            Path tmp = outDir1;
            outDir1 = outDir2;
            outDir2 = tmp;
        }
    }

    @Test
    public void testWords0() throws Throwable {
        //As one shortest transformation is "hit" -> "hot" -> "dot" -> "dog" -> "cog",
        assertEquals(5, words("hit", "cog", Arrays.asList("hot", "dot", "dog", "hit", "lot", "log", "cog")));
    }

    @Test
    public void testWords1() throws Throwable {
        assertEquals(11, words("cet", "ism", readFromResources("words1.txt")));
    }

    @Test
    public void testWords2() throws Throwable {
        assertEquals(11, words("sand", "acne", readFromResources("words2.txt")));
    }

    @Test
    public void testWords3() throws Throwable {
        assertEquals(0, words("hit", "cag", Arrays.asList("hot", "dot", "dog", "hit", "lot", "log", "cog")));
    }

}

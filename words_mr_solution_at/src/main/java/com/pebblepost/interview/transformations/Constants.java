package com.pebblepost.interview.transformations;

/** Common constants for Hadoop Jobs. */
public final class Constants {

    public static final String TRANSFORMATION_JOB_NAME = "Transformation Job";
    public static final String TRANSFORMATION_OUTPUT_DIR = "testing/transformation/output";

    public static final String GRAPH_JOB_NAME = "Graph Job";
    public static final String GRAPH_OUTPUT_DIR = "testing/graph/output";

    public static final String SHORTEST_WAY_JOB_NAME = "Shortest Way Job";
    public static final String SHORTEST_WAY_OUTPUT_DIR = "testing/shortestway/output";

    public static final String GET_SHORTEST_WAY_JOB_NAME = "Get Shortest Way Job";
    public static final String GET_SHORTEST_WAY_OUTPUT_DIR = "testing/getshortestway/output";

    public static final String PARAM_BEGIN_WORD = "com.pebblepost.transformations.beginWord";
    public static final String PARAM_END_WORD = "com.pebblepost.transformations.endWord";

    public static enum Counters {
        NOT_REACHED_NODES_COUNT,
        SHORTEST_WAY,
        ERROR_NODE_NOT_FOUND
    }

    private Constants() {
    }
}

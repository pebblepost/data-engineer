package com.pebblepost.interview.transformations;

import com.pebblepost.interview.transformations.model.AdjacencyNodes;
import com.pebblepost.interview.transformations.model.Node;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.Collections;

import static org.apache.commons.lang.StringUtils.isEmpty;

import static java.util.stream.StreamSupport.stream;

/**
 * Classes for Hadoop Job to construct graph, which is used for short for Shortest Path search.
 * Shortest Path is the shortest transformation from begin to end word.
 * <p>
 * The input for Graph job is taken from Transformation job.
 * The output is used by Shortest Way job.
 * <p>
 * <li>The output key: graph node id.
 * <li>The output value: <code>Node</code>,
 *     which includes adjacency node ids, initial distance, initial shortest path nodes.
 * <p>
 * 
 * @see Transformation
 * @see ShortestWay
 * @see Node
 */
public class Graph {

    public static class GraphMapper extends Mapper<Text, AdjacencyNodes, Text, AdjacencyNodes> {
        @Override
        protected void map(Text node, AdjacencyNodes value, Context context) throws IOException, InterruptedException {
            context.write(node, value);
        }
    }

    public static class GraphCombiner extends Reducer<Text, AdjacencyNodes, Text, AdjacencyNodes> {
        @Override
        protected void reduce(Text nodeId, Iterable<AdjacencyNodes> values, Context context)
                throws IOException, InterruptedException {
            AdjacencyNodes reducedNodes = new AdjacencyNodes();
            stream(values.spliterator(), false).forEach(nodes -> reducedNodes.merge(nodes));
            context.write(nodeId, reducedNodes);
        }
    }

    public static class GraphReducer extends Reducer<Text, AdjacencyNodes, Text, Node> {
        private String beginWord;

        @Override
        protected void setup(Context context) throws IOException, InterruptedException {
            beginWord = context.getConfiguration().get(Constants.PARAM_BEGIN_WORD);
            if (isEmpty(beginWord)) {
                throw new IllegalArgumentException("beginWord is not specified");
            }
        }

        @Override
        protected void reduce(Text nodeId, Iterable<AdjacencyNodes> values, Context context)
                throws IOException, InterruptedException {
            String id = nodeId.toString();
            AdjacencyNodes reducedNodes = new AdjacencyNodes();
            stream(values.spliterator(), false).forEach(nodes -> reducedNodes.merge(nodes));
            int distance = id.equals(beginWord) ? 0 : Integer.MAX_VALUE;

            context.write(nodeId, new Node(id, reducedNodes, distance, Collections.emptySet()));
        }
    }

}

package com.pebblepost.interview.transformations;

import com.pebblepost.interview.transformations.Constants.Counters;
import com.pebblepost.interview.transformations.model.Node;
import com.pebblepost.interview.transformations.model.ShortestWayValue;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import static org.apache.commons.lang.StringUtils.isEmpty;

/**
 * Classes for Hadoop Job to find Shortest Path in the graph from specified node to all others.
 * <p>
 * The input for this job it the output of Graph job.
 * The output is graph, each node contains shortest path length, and shortest path from specified node.
 * <p>
 * @see Graph
 */
public class ShortestWay {

    public static class ShortestWayMapper extends Mapper<Text, Node, Text, ShortestWayValue> {
        @Override
        protected void map(Text nodeId, Node node, Context context) throws IOException, InterruptedException {
            context.write(nodeId, new ShortestWayValue(node));

            if (node.getDistance() != Integer.MAX_VALUE) {
                for (String id : node.getAdjacencyNode().getNodeIds()) {
                    int distance = node.getDistance() + 1;
                    Set<String> parentNodeIds = new LinkedHashSet<>(node.getParentNodeIds());
                    parentNodeIds.add(node.getId());
                    ShortestWayValue value = new ShortestWayValue(distance, parentNodeIds);
                    context.write(new Text(id), value);
                }
            }
        }
    }

    public static class ShortestWayCombiner extends Reducer<Text, ShortestWayValue, Text, ShortestWayValue> {
        @Override
        protected void reduce(Text nodeId, Iterable<ShortestWayValue> values, Context context)
                throws IOException, InterruptedException {
            Node node = resolveDistances(values, context);
            if (node != null) {
                context.write(nodeId, new ShortestWayValue(node));
            }
        }
    }

    public static class ShortestWayReducer extends Reducer<Text, ShortestWayValue, Text, Node> {
        @Override
        protected void reduce(Text nodeId, Iterable<ShortestWayValue> values, Context context)
                throws IOException, InterruptedException {
            Node node = resolveDistances(values, context);
            if (node != null) {
                context.write(nodeId, node);
            }
        }
    }

    @SuppressWarnings("rawtypes")
    private static Node resolveDistances(Iterable<ShortestWayValue> values, Context context) {
        Node node = null;
        int minDistance = Integer.MAX_VALUE;
        Set<String> parentNodeIds = Collections.emptySet();

        for (ShortestWayValue value : values) {
            if (value.getNode() != null) {
                node = value.getNode();
                if (minDistance > node.getDistance()) {
                    parentNodeIds = node.getParentNodeIds();
                    minDistance = node.getDistance();
                }
            } else {
                if (minDistance > value.getDistance()) {
                    parentNodeIds = value.getParentNodeIds();
                    minDistance = value.getDistance();
                }
            }
        }

        if (minDistance == Integer.MAX_VALUE) {
            context.getCounter(Counters.NOT_REACHED_NODES_COUNT).increment(1);
        }

        if (node != null) {
            return new Node(node.getId(), node.getAdjacencyNode(), minDistance, parentNodeIds);

        } else {
            // should not be happen
            context.getCounter(Counters.ERROR_NODE_NOT_FOUND).increment(1);
            return null;
        }
    }

    public static class GetShortestWayMapper extends Mapper<Text, Node, NullWritable, NullWritable> {
        private String endWord;

        @Override
        protected void setup(Context context) throws IOException, InterruptedException {
            endWord = context.getConfiguration().get(Constants.PARAM_END_WORD);
            if (isEmpty(endWord)) {
                throw new IllegalArgumentException("endWord is not specified");
            }
        }

        @Override
        protected void map(Text nodeId, Node node, Context context) throws IOException, InterruptedException {
            if (nodeId.toString().equals(endWord)) {
                context.getCounter(Counters.SHORTEST_WAY).setValue(node.getDistance());
            }
        }
    }

}

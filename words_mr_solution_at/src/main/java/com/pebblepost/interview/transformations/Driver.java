package com.pebblepost.interview.transformations;

import com.pebblepost.interview.transformations.Constants.Counters;
import com.pebblepost.interview.transformations.ShortestWay.ShortestWayCombiner;
import com.pebblepost.interview.transformations.model.AdjacencyNodes;
import com.pebblepost.interview.transformations.model.Node;
import com.pebblepost.interview.transformations.model.ShortestWayValue;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;

import static org.apache.hadoop.io.SequenceFile.Reader.file;

/** Driver class for managing Hadoop Jobs. */
public class Driver {

    public static final Logger logger = LoggerFactory.getLogger(Driver.class);

    private FileSystem fs;
    private Configuration conf;

    public Driver(FileSystem fs, Configuration conf) {
        this.fs = fs;
        this.conf = conf;
    }

    public Path run(String beginWord, Path inDir) throws IOException, InterruptedException {
        buildGraph(beginWord, inDir);
        return runShortestWayJob();
    }

    private void buildGraph(String beginWord, Path inDir) throws IOException, InterruptedException {
        logger.debug("Build graph");

        runTransformationJob(beginWord, inDir);
        runGraphJob(beginWord);
    }

    private void runTransformationJob(String beginWord, Path inDir) throws IOException, InterruptedException {
        logger.debug("Run {}", Constants.TRANSFORMATION_JOB_NAME);
        conf.set(Constants.PARAM_BEGIN_WORD, beginWord);

        Job job = Job.getInstance(conf, Constants.TRANSFORMATION_JOB_NAME);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(AdjacencyNodes.class);
        job.setMapperClass(Transformation.TransformationMapper.class);
        job.setCombinerClass(Transformation.TransformationCombiner.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(AdjacencyNodes.class);
        job.setReducerClass(Transformation.TransformationReducer.class);
        job.setOutputFormatClass(SequenceFileOutputFormat.class);

        Path outDir = new Path(Constants.TRANSFORMATION_OUTPUT_DIR);
        fs.delete(outDir, true);
        FileInputFormat.setInputPaths(job, inDir);
        FileOutputFormat.setOutputPath(job, outDir);

        try {
            job.waitForCompletion(true);
        } catch (ClassNotFoundException | InterruptedException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
        logger.debug("{} state: {}", Constants.TRANSFORMATION_JOB_NAME, job.getStatus().getState());
    }

    private void runGraphJob(String beginWord) throws IOException, InterruptedException {
        logger.debug("Run {}", Constants.GRAPH_JOB_NAME);
        conf.set(Constants.PARAM_BEGIN_WORD, beginWord);

        Job job = Job.getInstance(conf, Constants.GRAPH_JOB_NAME);
        job.setInputFormatClass(SequenceFileInputFormat.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(AdjacencyNodes.class);
        job.setMapperClass(Graph.GraphMapper.class);
        job.setCombinerClass(Graph.GraphCombiner.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Node.class);
        job.setReducerClass(Graph.GraphReducer.class);
        job.setOutputFormatClass(SequenceFileOutputFormat.class);

        Path inDir = new Path(Constants.TRANSFORMATION_OUTPUT_DIR);
        Path outDir = new Path(Constants.GRAPH_OUTPUT_DIR);
        fs.delete(outDir, true);
        FileInputFormat.setInputPaths(job, inDir);
        FileOutputFormat.setOutputPath(job, outDir);

        try {
            job.waitForCompletion(true);
        } catch (ClassNotFoundException | InterruptedException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
        logger.debug("{} state: {}", Constants.GRAPH_JOB_NAME, job.getStatus().getState());
    }

    private Path runShortestWayJob() throws IOException, InterruptedException {
        long prevNotReachedNodesCount = Long.MAX_VALUE;
        long notReachedNodesCount = Long.MAX_VALUE;
        int iteration = 1;

        Path inDir = new Path(Constants.GRAPH_OUTPUT_DIR);
        Path outDir = new Path(Constants.SHORTEST_WAY_OUTPUT_DIR + iteration);

        // iterative processing
        // call job unless distance to all nodes are calculated
        // it is used Hadoop MapReduce counter to check how many unresolved nodes are left
        // prevNotReachedNodesCount is used for case when some nodes are not reachable,
        // in this case next iteration doesn't have any effect
        while (notReachedNodesCount != 0 || notReachedNodesCount != prevNotReachedNodesCount) {
            logger.debug("Run {} iteration {}", Constants.SHORTEST_WAY_JOB_NAME, iteration);
            fs.delete(outDir, true);
            prevNotReachedNodesCount = notReachedNodesCount;

            Job job = Job.getInstance(conf, Constants.SHORTEST_WAY_JOB_NAME);
            job.setInputFormatClass(SequenceFileInputFormat.class);
            job.setMapOutputKeyClass(Text.class);
            job.setMapOutputValueClass(ShortestWayValue.class);
            job.setMapperClass(ShortestWay.ShortestWayMapper.class);
            job.setCombinerClass(ShortestWayCombiner.class);
            job.setOutputKeyClass(Text.class);
            job.setOutputValueClass(Node.class);
            job.setReducerClass(ShortestWay.ShortestWayReducer.class);
            job.setOutputFormatClass(SequenceFileOutputFormat.class);

            FileInputFormat.setInputPaths(job, inDir);
            FileOutputFormat.setOutputPath(job, outDir);

            try {
                job.waitForCompletion(true);
            } catch (ClassNotFoundException | InterruptedException ex) {
                throw new RuntimeException(ex.getMessage(), ex);
            }

            logger.debug("{} iteration {} state: {}", Constants.SHORTEST_WAY_JOB_NAME, iteration,
                    job.getStatus().getState());
            Counter counter = job.getCounters().findCounter(Counters.NOT_REACHED_NODES_COUNT);
            notReachedNodesCount = counter == null ? 0 : counter.getValue();
            logger.debug("{} iteration {} not reached nodes count: {} ", Constants.SHORTEST_WAY_JOB_NAME, iteration,
                    notReachedNodesCount);

            inDir = new Path(Constants.SHORTEST_WAY_OUTPUT_DIR + iteration);
            iteration++;
            outDir = new Path(Constants.SHORTEST_WAY_OUTPUT_DIR + iteration);
        }

        return inDir;
    }

    public int getShortestWay(String endWord, Path inDir) throws IOException {
        // current implementation leverage MapReduce counter to find shortest path length
        // alternative way it is manually read HDFS files and find endWord node, get distance from it,
        // as it is done in the getShortestWayPath() method
        logger.debug("Get shortest way for {}", endWord);
        conf.set(Constants.PARAM_END_WORD, endWord);

        Job job = Job.getInstance(conf, Constants.GET_SHORTEST_WAY_JOB_NAME);
        job.setInputFormatClass(SequenceFileInputFormat.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Node.class);
        job.setMapperClass(ShortestWay.GetShortestWayMapper.class);
        job.setNumReduceTasks(0);
        job.setOutputKeyClass(NullWritable.class);
        job.setOutputValueClass(NullWritable.class);

        Path outDir = new Path(Constants.GET_SHORTEST_WAY_OUTPUT_DIR);
        fs.delete(outDir, true);
        FileInputFormat.setInputPaths(job, inDir);
        FileOutputFormat.setOutputPath(job, outDir);

        try {
            job.waitForCompletion(true);
        } catch (ClassNotFoundException | InterruptedException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }

        Counter counter = job.getCounters().findCounter(Counters.SHORTEST_WAY);
        // test expects number of nodes instead number of edges
        return (int) (counter == null ? 0 : (counter.getValue() + 1));
    }

    public String getShortestWayPath(String endWord, Path inDir) throws IOException {
        logger.debug("Get shortest way path for {}", endWord);
        FileStatus[] fileStatus = fs.listStatus(inDir);
        for (FileStatus file : fileStatus) {
            String name = file.getPath().getName();
            if (name.contains("part-r-")) {
                Text key = new Text();
                Node node = new Node();
                try (SequenceFile.Reader reader = new SequenceFile.Reader(conf, file(file.getPath()))) {
                    while (reader.next(key, node)) {
                        if (key.toString().equals(endWord)) {
                            Set<String> nodeIds = new LinkedHashSet<>(node.getParentNodeIds());
                            nodeIds.add(node.getId());
                            return node.getDistance() == Integer.MAX_VALUE ? "" : nodeIds.toString();
                        }
                    }
                }
            }
        }

        return "";
    }
}

package com.pebblepost.interview.transformations;

import com.pebblepost.interview.transformations.model.AdjacencyNodes;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

import static org.apache.commons.collections.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang.StringUtils.isEmpty;

import static java.util.stream.StreamSupport.stream;

/**
 * Classes for auxiliary Hadoop Job to construct graph.
 * Output is used by Graph Hadoop Job.
 * <p>
 * Input for the job is dictionary word. Words are grouped by common transformation key.
 * For example, `hit` and `hot` words have `h_t` common transformation key.
 * The business rules is: Only one letter can be changed at a time.
 * So for `hit` transformation keys are: `_it`, `h_t`, `hi_`.
 * <p>
 * Output of the job:
 * <li>key: node id
 * <li>sub list of adjacency nodes
 * <p>
 * Full list of adjacency nodes will be constructed during Graph job. Join by node id (comparing to transformation key)
 * is required.
 * <p>
 * 
 * @see Graph
 */
public class Transformation {

    public static class TransformationMapper extends Mapper<Object, Text, Text, AdjacencyNodes> {
        @Override
        protected void setup(Context context) throws IOException, InterruptedException {
            String beginWord = context.getConfiguration().get(Constants.PARAM_BEGIN_WORD);
            if (isEmpty(beginWord)) {
                throw new IllegalArgumentException("beginWord is not specified");
            }

            mapWord(beginWord, context);
        }

        @Override
        protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            mapWord(value.toString(), context);
        }

        private void mapWord(String word, Context context) throws IOException, InterruptedException {
            if (isEmpty(word)) {
                return;
            }

            AdjacencyNodes adjacencyNodes = new AdjacencyNodes(word);

            for (int i = 0; i < word.length(); i++) {
                // for `hit` transformation keys are: `_it`, `h_t`, `hi_`
                String transformationKey = transformationKey(word, i);
                context.write(new Text(transformationKey), adjacencyNodes);
            }
        }
    }

    public static class TransformationCombiner extends Reducer<Text, AdjacencyNodes, Text, AdjacencyNodes> {
        @Override
        protected void reduce(Text transofrmationKey, Iterable<AdjacencyNodes> values, Context context)
                throws IOException, InterruptedException {
            AdjacencyNodes reducedNodes = reduceAdjacencyNodes(values);
            context.write(transofrmationKey, reducedNodes);
        }

        protected AdjacencyNodes reduceAdjacencyNodes(Iterable<AdjacencyNodes> values) {
            AdjacencyNodes reducedNodes = new AdjacencyNodes();
            stream(values.spliterator(), false).forEach(nodes -> reducedNodes.merge(nodes));
            return reducedNodes;
        }
    }

    public static class TransformationReducer extends TransformationCombiner {
        @Override
        protected void reduce(Text transofrmationKey, Iterable<AdjacencyNodes> values, Context context)
                throws IOException, InterruptedException {
            AdjacencyNodes reducedNodes = reduceAdjacencyNodes(values);

            for (String nodeId : reducedNodes.getNodeIds()) {
                // node is not adjacency node for it self, build graph without hinge
                AdjacencyNodes adjacencyNodes = reducedNodes.withoutNode(nodeId);
                if (isNotEmpty(adjacencyNodes.getNodeIds())) {
                    context.write(new Text(nodeId), adjacencyNodes);
                }
            }
        }
    }

    private static String transformationKey(String word, int i) {
        StringBuilder sb = new StringBuilder(word);
        sb.setCharAt(i, '_');
        return sb.toString();
    }

}

package com.pebblepost.interview.transformations.model;

import com.pebblepost.interview.transformations.ShortestWay.ShortestWayMapper;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import static org.apache.commons.collections.CollectionUtils.isNotEmpty;
import static org.apache.commons.collections.CollectionUtils.size;

/**
 * Auxiliary class to uniform map output types for <code>ShortestWay.ShortestWayMapper</code>.
 * <p>
 * The first output is node itself. <code>Node</code>.
 * <p>
 * The second output data needed for calculating shortest way: distance, and list of parent nodes.
 * 
 * @see ShortestWayMapper
 */
public class ShortestWayValue implements Writable {

    private Node node;
    private int distance = Integer.MAX_VALUE;
    private Set<String> parentNodeIds = new LinkedHashSet<>();

    public ShortestWayValue(Node node) {
        this.node = node;
    }

    public ShortestWayValue(int distance, Set<String> parentNodeIds) {
        this.distance = distance;
        this.parentNodeIds = parentNodeIds;
    }

    ShortestWayValue() {
    }

    public Node getNode() {
        return node;
    }

    public int getDistance() {
        return distance;
    }

    public Set<String> getParentNodeIds() {
        return Collections.unmodifiableSet(parentNodeIds);
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeInt(distance);

        out.writeBoolean(node != null);
        if (node != null) {
            node.write(out);
        }

        out.writeInt(size(parentNodeIds));
        if (isNotEmpty(parentNodeIds)) {
            for (String parentNodeId : parentNodeIds) {
                out.writeUTF(parentNodeId);
            }
        }
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        distance = in.readInt();

        if (in.readBoolean()) {
            node = new Node();
            node.readFields(in);
        } else {
            node = null;
        }

        int size = in.readInt();
        parentNodeIds = new LinkedHashSet<>(size);
        for (int i = 0; i < size; i++) {
            parentNodeIds.add(in.readUTF());
        }
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(distance)
                .append(node)
                .append(parentNodeIds)
                .hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ShortestWayValue that = ((ShortestWayValue) obj);

        return new EqualsBuilder()
                .append(distance, that.distance)
                .append(node, that.node)
                .append(parentNodeIds, that.parentNodeIds)
                .isEquals();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE)
                .append("distance: ", distance)
                .append("node: ", node)
                .append("parentNodeIds: ", parentNodeIds)
                .toString();
    }

}

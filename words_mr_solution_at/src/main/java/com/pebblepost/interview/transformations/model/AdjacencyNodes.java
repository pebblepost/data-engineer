package com.pebblepost.interview.transformations.model;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static com.google.common.collect.Sets.difference;

import static java.util.Collections.singleton;

/**
 * Represents adjacency node list in the graph for particular node.
 */
public class AdjacencyNodes implements Writable {

    private Set<String> nodeIds = new HashSet<>();

    public AdjacencyNodes() {
    }

    public AdjacencyNodes(String nodeId) {
        this.nodeIds.add(nodeId);
    }

    public AdjacencyNodes(Set<String> nodeIds) {
        this.nodeIds.addAll(nodeIds);
    }

    public Set<String> getNodeIds() {
        return Collections.unmodifiableSet(nodeIds);
    }

    public void merge(AdjacencyNodes adjacencyNodes) {
        nodeIds.addAll(adjacencyNodes.getNodeIds());
    }

    public AdjacencyNodes withoutNode(String nodeId) {
        // TODO: optimize with collection view
        return new AdjacencyNodes(difference(nodeIds, singleton(nodeId)));
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeInt(nodeIds.size());
        for (String nodeId : nodeIds) {
            out.writeUTF(nodeId);
        }
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        int size = in.readInt();
        HashSet<String> result = new HashSet<>(size);
        for (int i = 0; i < size; i++) {
            result.add(in.readUTF());
        }
        this.nodeIds = result;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(nodeIds)
                .hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        AdjacencyNodes that = ((AdjacencyNodes) obj);

        return new EqualsBuilder()
                .append(nodeIds, that.nodeIds)
                .isEquals();
    }
    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE)
                .append("nodeIds: " + nodeIds)
                .toString();
    }
}

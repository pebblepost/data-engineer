package com.pebblepost.interview.transformations.model;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import static org.apache.commons.collections.CollectionUtils.isNotEmpty;
import static org.apache.commons.collections.CollectionUtils.size;

/** Represents graph node. */
public class Node implements WritableComparable<Node> {

    private String id;
    private AdjacencyNodes adjacencyNodes = new AdjacencyNodes();

    /** Shortest way length. */
    private int distance = Integer.MAX_VALUE;
    /** Shortest way path. */
    private Set<String> parentNodeIds = new LinkedHashSet<>();

    public Node(String id) {
        this.id = id;
    }

    public Node(String id, AdjacencyNodes adjacencyNodes, int distance, Set<String> parentNodeIds) {
        this.id = id;
        this.adjacencyNodes = adjacencyNodes;
        this.distance = distance;
        this.parentNodeIds = parentNodeIds;
    }

    public Node() {
    }

    public String getId() {
        return id;
    }

    public AdjacencyNodes getAdjacencyNode() {
        return adjacencyNodes;
    }

    public int getDistance() {
        return distance;
    }

    public Set<String> getParentNodeIds() {
        return Collections.unmodifiableSet(parentNodeIds);
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeUTF(id);
        adjacencyNodes.write(out);
        out.writeInt(distance);

        out.writeInt(size(parentNodeIds));
        if (isNotEmpty(parentNodeIds)) {
            for (String parentNodeId : parentNodeIds) {
                out.writeUTF(parentNodeId);
            }
        }
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        id = in.readUTF();
        adjacencyNodes.readFields(in);
        distance = in.readInt();

        int size = in.readInt();
        parentNodeIds = new LinkedHashSet<>(size);
        for (int i = 0; i < size; i++) {
            parentNodeIds.add(in.readUTF());
        }
    }

    @Override
    public int compareTo(Node node) {
        int distanceComparison = Integer.compare(distance, node.distance);
        return distanceComparison == 0 ? id.compareTo(node.id) : distanceComparison;
        
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(id)
                .append(adjacencyNodes)
                .hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Node that = ((Node) obj);

        return new EqualsBuilder()
                .append(id, that.id)
                .append(adjacencyNodes, that.adjacencyNodes)
                .isEquals();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE)
                .append("id: ", id)
                .append("adjacencyNodes: ", adjacencyNodes)
                .append("distance: ", distance)
                .append("parentNodeIds: ", parentNodeIds)
                .toString();
    }
}

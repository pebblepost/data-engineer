package com.pebblepost.interview;

import com.pebblepost.interview.transformations.Driver;

import org.apache.hadoop.fs.Path;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class MainTest extends AbstractMiniDFSClusterTest {

    public static final Logger logger = LoggerFactory.getLogger(MainTest.class);

    private static final Path IN_DIR = new Path("testing/words/input");
    private static final String DATA_FILE = "sample.txt";

    /**
     * 0) Read WordCount.java and WordCountTest.java to understand how to use testing environment
     * 1) Save the wordList to the HDFS
     * 2) Run your hadoop jobs
     * 3) Read the result from the HDFS
     * 4) Return the result
     *
     * @param beginWord the starting word e.g. "hit"
     * @param endWord   the ending word e.g. "cog"
     * @param wordList  the dictionary of all allowed words e.g. ["hot","dot","dog","lot","log","cog"]
     * @return the length of one shortest transformation e.g. "hit" -> "hot" -> "dot" -> "dog" -> "cog"
     */
    int words(String beginWord, String endWord, List<String> wordList) throws Throwable {
        logger.info("Find shortest transformation for {} -> {}", beginWord, endWord);
        saveWordList(wordList);
        Path outDir = runHadoopJobs(beginWord);
        return readResults(endWord, outDir);
    }

    @Test
    public void testWords0() throws Throwable {
        //As one shortest transformation is "hit" -> "hot" -> "dot" -> "dog" -> "cog",
        assertEquals(5, words("hit", "cog", Arrays.asList("hot", "dot", "dog", "hit", "lot", "log", "cog")));
    }

    @Test
    public void testWords1() throws Throwable {
        assertEquals(11, words("cet", "ism", readFromResources("words1.txt")));
    }

    @Test
    public void testWords2() throws Throwable {
        assertEquals(11, words("sand", "acne", readFromResources("words2.txt")));
    }

    private void saveWordList(List<String> wordList) throws IOException {
        logger.debug("Save word list to HDFS");
        fs.delete(IN_DIR, true);

        writeHDFSContent(fs, IN_DIR, DATA_FILE, wordList);
    }

    private Path runHadoopJobs(String beginWord) throws IOException, InterruptedException {
        logger.debug("Run Hadoop Jobs");
        return new Driver(fs, conf).run(beginWord, IN_DIR);
    }

    private int readResults(String endWord, Path outDir) throws Exception {
        logger.debug("Read results");

        if (!fs.exists(outDir)) {
            logger.info("{} doesn't exist", outDir);
            return 0;
        }

        Driver driver = new Driver(fs, conf);
        int shortestWay = driver.getShortestWay(endWord, outDir);
        String shortestWayPath = driver.getShortestWayPath(endWord, outDir);
        logger.info("Shortest transformation length: {}", shortestWay);
        logger.info("Shortest transformation path: {}", shortestWayPath);

        return shortestWay;
    }
}

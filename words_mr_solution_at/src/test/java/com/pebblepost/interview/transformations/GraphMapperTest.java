package com.pebblepost.interview.transformations;

import com.google.common.collect.ImmutableSet;
import com.pebblepost.interview.transformations.Graph.GraphMapper;
import com.pebblepost.interview.transformations.model.AdjacencyNodes;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.types.Pair;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class GraphMapperTest {

    private MapDriver<Text, AdjacencyNodes, Text, AdjacencyNodes> mapDriver;

    @Before
    public void setUp() {
        mapDriver = MapDriver.newMapDriver(new GraphMapper());
    }

    @Test
    public void testMap() throws IOException {
        Pair<Text, AdjacencyNodes> pair = new Pair<>(new Text("dog"), new AdjacencyNodes(ImmutableSet.of("cog", "log")));

        mapDriver.withInput(pair)
                 .withOutput(pair)
                 .runTest();
    }

}

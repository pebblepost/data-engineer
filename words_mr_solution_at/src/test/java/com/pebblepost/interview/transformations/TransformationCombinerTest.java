package com.pebblepost.interview.transformations;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.pebblepost.interview.transformations.Transformation.TransformationCombiner;
import com.pebblepost.interview.transformations.model.AdjacencyNodes;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.apache.hadoop.mrunit.types.Pair;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class TransformationCombinerTest {

    private ReduceDriver<Text, AdjacencyNodes, Text, AdjacencyNodes> reduceDriver;

    @Before
    public void setUp() {
        reduceDriver = ReduceDriver.newReduceDriver(new TransformationCombiner());
    }

    @Test
    public void testReduceAdjacencyNodes() throws IOException {
        reduceDriver.withInput(new Pair<>(new Text("_og"), ImmutableList.of(
                                                            new AdjacencyNodes(ImmutableSet.of("dog")),
                                                            new AdjacencyNodes(ImmutableSet.of("cog", "log")))))
                    .withOutput(new Text("_og"), new AdjacencyNodes(ImmutableSet.of("dog", "cog", "log")))
                    .runTest();
    }

}

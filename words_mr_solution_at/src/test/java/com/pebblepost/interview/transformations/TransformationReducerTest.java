package com.pebblepost.interview.transformations;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.pebblepost.interview.transformations.Transformation.TransformationReducer;
import com.pebblepost.interview.transformations.model.AdjacencyNodes;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.apache.hadoop.mrunit.types.Pair;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class TransformationReducerTest {

    private ReduceDriver<Text, AdjacencyNodes, Text, AdjacencyNodes> reduceDriver;

    @Before
    public void setUp() {
        reduceDriver = ReduceDriver.newReduceDriver(new TransformationReducer());
    }

    @Test
    public void testReduceAdjacencyNodes() throws IOException {
        reduceDriver.withInput(new Pair<>(new Text("_og"), ImmutableList.of(
                                                            new AdjacencyNodes(ImmutableSet.of("dog", "cog", "log")))))
                    .withOutput(new Text("dog"), new AdjacencyNodes(ImmutableSet.of("cog", "log")))
                    .withOutput(new Text("cog"), new AdjacencyNodes(ImmutableSet.of("dog", "log")))
                    .withOutput(new Text("log"), new AdjacencyNodes(ImmutableSet.of("dog", "cog")))
                    .runTest(false);
    }

    @Test
    public void testReduceSingleAdjacencyNodes() throws IOException {
        // 'dog' is not adjacency node for it self, build graph without hinge
        reduceDriver.withInput(new Pair<>(new Text("_og"), ImmutableList.of(new AdjacencyNodes(ImmutableSet.of("dog")))))
                    .runTest(false);
    }

}

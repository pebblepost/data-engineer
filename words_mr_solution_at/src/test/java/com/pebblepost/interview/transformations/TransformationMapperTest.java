package com.pebblepost.interview.transformations;

import com.pebblepost.interview.transformations.Transformation.TransformationMapper;
import com.pebblepost.interview.transformations.model.AdjacencyNodes;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.types.Pair;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class TransformationMapperTest {

    private MapDriver<Object, Text, Text, AdjacencyNodes> mapDriver;

    @Before
    public void setUp() {
        mapDriver = MapDriver.newMapDriver(new TransformationMapper());
        mapDriver.getConfiguration().set(Constants.PARAM_BEGIN_WORD, "hit");
    }

    @Test
    public void testMapWord() throws IOException {
        mapDriver.withInput(new Pair<>(NullWritable.get(), new Text("cog")))
                 .withOutput(new Pair<>(new Text("_it"), new AdjacencyNodes("hit")))
                 .withOutput(new Pair<>(new Text("h_t"), new AdjacencyNodes("hit")))
                 .withOutput(new Pair<>(new Text("hi_"), new AdjacencyNodes("hit")))
                 .withOutput(new Pair<>(new Text("_og"), new AdjacencyNodes("cog")))
                 .withOutput(new Pair<>(new Text("c_g"), new AdjacencyNodes("cog")))
                 .withOutput(new Pair<>(new Text("co_"), new AdjacencyNodes("cog")))
                 .runTest(false);
    }

    @Test
    public void testMapWordWithBeginWordDuplicate() throws IOException {
        // note: duplicates will be removed during combine/reduce phase
        mapDriver.withInput(new Pair<>(NullWritable.get(), new Text("hit")))
                 .withOutput(new Pair<>(new Text("_it"), new AdjacencyNodes("hit")))
                 .withOutput(new Pair<>(new Text("h_t"), new AdjacencyNodes("hit")))
                 .withOutput(new Pair<>(new Text("hi_"), new AdjacencyNodes("hit")))
                 .withOutput(new Pair<>(new Text("_it"), new AdjacencyNodes("hit")))
                 .withOutput(new Pair<>(new Text("h_t"), new AdjacencyNodes("hit")))
                 .withOutput(new Pair<>(new Text("hi_"), new AdjacencyNodes("hit")))
                 .runTest(false);
    }

    @Test
    public void testMapEmptyWord() throws IOException {
        // note: duplicates will be removed during combine/reduce phase
        mapDriver.withInput(new Pair<>(NullWritable.get(), new Text("")))
                 .withOutput(new Pair<>(new Text("_it"), new AdjacencyNodes("hit")))
                 .withOutput(new Pair<>(new Text("h_t"), new AdjacencyNodes("hit")))
                 .withOutput(new Pair<>(new Text("hi_"), new AdjacencyNodes("hit")))
                 .runTest(false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullBeginWord() throws IOException {
        mapDriver.getConfiguration().set(Constants.PARAM_BEGIN_WORD, null);

        mapDriver.withInput(new Pair<>(NullWritable.get(), new Text("cog")))
                 .runTest(false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyBeginWord() throws IOException {
        mapDriver.getConfiguration().set(Constants.PARAM_BEGIN_WORD, "");

        mapDriver.withInput(new Pair<>(NullWritable.get(), new Text("cog")))
                 .runTest(false);
    }
}

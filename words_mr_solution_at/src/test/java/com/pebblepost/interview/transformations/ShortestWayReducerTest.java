package com.pebblepost.interview.transformations;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.pebblepost.interview.transformations.Constants.Counters;
import com.pebblepost.interview.transformations.ShortestWay.ShortestWayReducer;
import com.pebblepost.interview.transformations.model.AdjacencyNodes;
import com.pebblepost.interview.transformations.model.Node;
import com.pebblepost.interview.transformations.model.ShortestWayValue;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.apache.hadoop.mrunit.types.Pair;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class ShortestWayReducerTest {

    private ReduceDriver<Text, ShortestWayValue, Text, Node> reduceDriver;

    @Before
    public void setUp() {
        reduceDriver = ReduceDriver.newReduceDriver(new ShortestWayReducer());
    }

    @Test
    public void testReduceNotReachedNode() throws IOException {
        reduceDriver.getCounters().findCounter(Counters.NOT_REACHED_NODES_COUNT).setValue(3);

        Text nodeId = new Text("dog");
        Node node = new Node("dog",
                new AdjacencyNodes(ImmutableSet.of("dot", "cog", "log")),
                Integer.MAX_VALUE,  // distance == maximum for not reached node
                ImmutableSet.of()); // shortest path is empty

        reduceDriver.withInput(new Pair<>(nodeId, ImmutableList.of(new ShortestWayValue(node))))
                    .withOutput(new Pair<Text, Node>(nodeId, node)) // pass node as is
                    .withCounter(Counters.NOT_REACHED_NODES_COUNT, 4)
                    .runTest();
    }

    @Test
    public void testNodeHasMinDistance() throws IOException {
        Text nodeId = new Text("dog");
        Node node = new Node("dog",
                new AdjacencyNodes(ImmutableSet.of("dot", "cog", "log")),
                3, // distance
                ImmutableSet.of("hit", "hot", "dot"));

        reduceDriver.withInput(new Pair<>(nodeId, ImmutableList.of(
                                                    new ShortestWayValue(node),
                                                    new ShortestWayValue(4, ImmutableSet.of("hit", "hot", "dot", "dog")),
                                                    new ShortestWayValue(5, ImmutableSet.of("hit", "hot", "dot", "dog", "fog")))))
                    .withOutput(new Pair<Text, Node>(nodeId, node))
                    .runTest();
    }

    @Test
    public void testDistanceListHasMinDistance() throws IOException {
        Text nodeId = new Text("dog");
        Node node = new Node("dog",
                new AdjacencyNodes(ImmutableSet.of("dot", "cog", "log")),
                4, // distance
                ImmutableSet.of("hit", "hot", "dot", "dog"));

        reduceDriver.withInput(new Pair<>(nodeId, ImmutableList.of(
                                                    new ShortestWayValue(node),
                                                    new ShortestWayValue(3, ImmutableSet.of("hit", "hot", "dot")),
                                                    new ShortestWayValue(5, ImmutableSet.of("hit", "hot", "dot", "dog", "fog")))))
                    .withOutput(new Pair<Text, Node>(nodeId, 
                            new Node(node.getId(), node.getAdjacencyNode(), 3, ImmutableSet.of("hit", "hot", "dot"))))
                    .runTest();
    }

    @Test
    public void testNodeNotFound() throws IOException {
        Text nodeId = new Text("dog");
        ShortestWayValue value = new ShortestWayValue(3, ImmutableSet.of("dot", "cog", "log"));
        reduceDriver.getCounters().findCounter(Counters.ERROR_NODE_NOT_FOUND).setValue(3);

        reduceDriver.withInput(new Pair<>(nodeId, ImmutableList.of(value)))
                    .withCounter(Counters.ERROR_NODE_NOT_FOUND, 4)
                    .runTest();
    }

}

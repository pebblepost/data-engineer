package com.pebblepost.interview.transformations;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.pebblepost.interview.transformations.Graph.GraphCombiner;
import com.pebblepost.interview.transformations.model.AdjacencyNodes;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.apache.hadoop.mrunit.types.Pair;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class GraphCombinerTest {

    private ReduceDriver<Text, AdjacencyNodes, Text, AdjacencyNodes> reduceDriver;

    @Before
    public void setUp() {
        reduceDriver = ReduceDriver.newReduceDriver(new GraphCombiner());
    }

    @Test
    public void testReduceAdjacencyNodes() throws IOException {
        reduceDriver.withInput(new Pair<>(new Text("dog"), ImmutableList.of(
                                                            new AdjacencyNodes(ImmutableSet.of("dot")),
                                                            new AdjacencyNodes(ImmutableSet.of("cog", "log")))))
                    .withOutput(new Text("dog"), new AdjacencyNodes(ImmutableSet.of("dot", "cog", "log")))
                    .runTest();
    }

}

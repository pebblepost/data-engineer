package com.pebblepost.interview.transformations;

import com.google.common.collect.ImmutableSet;
import com.pebblepost.interview.transformations.ShortestWay.ShortestWayMapper;
import com.pebblepost.interview.transformations.model.AdjacencyNodes;
import com.pebblepost.interview.transformations.model.Node;
import com.pebblepost.interview.transformations.model.ShortestWayValue;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.types.Pair;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class ShortestWayMapperTest {

    private MapDriver<Text, Node, Text, ShortestWayValue> mapDriver;

    @Before
    public void setUp() {
        mapDriver = MapDriver.newMapDriver(new ShortestWayMapper());
    }

    @Test
    public void testMapNotReachedNode() throws IOException {
        Text nodeId = new Text("dog");
        Node node = new Node("dog",
                new AdjacencyNodes(ImmutableSet.of("dot", "cog", "log")),
                Integer.MAX_VALUE,  // distance == maximum for not reached node
                ImmutableSet.of()); // shortest path is empty

        mapDriver.withInput(new Pair<>(nodeId, node))
                 .withOutput(new Pair<>(nodeId, new ShortestWayValue(node)))
                 .runTest();
    }

    public void testMapReachedNode() throws IOException {
        Text nodeId = new Text("dog");
        Node node = new Node("dog",
                new AdjacencyNodes(ImmutableSet.of("dot", "cog", "log")),
                3, // distance != maximum for reached node
                ImmutableSet.of("hit", "hot", "dot")); // shortest path to reached node

        mapDriver.withInput(new Pair<>(nodeId, node))
                 .withOutput(new Pair<>(nodeId, new ShortestWayValue(node)))
                 .withOutput(new Pair<>(new Text("dot"), new ShortestWayValue(4, ImmutableSet.of("hit", "hot", "dot")))) // will be eliminated during reduce phase
                 .withOutput(new Pair<>(new Text("cog"), new ShortestWayValue(4, ImmutableSet.of("hit", "hot", "dot", "cog"))))
                 .withOutput(new Pair<>(new Text("log"), new ShortestWayValue(4, ImmutableSet.of("hit", "hot", "dot", "log"))))
                 .runTest(false);
    }

}

package com.pebblepost.interview.transformations;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.pebblepost.interview.transformations.Graph.GraphReducer;
import com.pebblepost.interview.transformations.model.AdjacencyNodes;
import com.pebblepost.interview.transformations.model.Node;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.apache.hadoop.mrunit.types.Pair;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class GraphReducerTest {

    private ReduceDriver<Text, AdjacencyNodes, Text, Node> reduceDriver;

    @Before
    public void setUp() {
        reduceDriver = ReduceDriver.newReduceDriver(new GraphReducer());
        reduceDriver.getConfiguration().set(Constants.PARAM_BEGIN_WORD, "hit");
    }

    @Test
    public void testReduce() throws IOException {
        Text nodeId = new Text("dog");
        Node node = new Node("dog",
                             new AdjacencyNodes(ImmutableSet.of("dot", "cog", "log")),
                             Integer.MAX_VALUE, // distance == maximum for non-begin node
                             ImmutableSet.of());

        reduceDriver.withInput(new Pair<>(nodeId, ImmutableList.of(
                                                            new AdjacencyNodes(ImmutableSet.of("dot")),
                                                            new AdjacencyNodes(ImmutableSet.of("cog", "log")))))
                    .withOutput(nodeId, node)
                    .runTest();
    }

    @Test
    public void testReduceBeginWordNode() throws IOException {
        reduceDriver.getConfiguration().set(Constants.PARAM_BEGIN_WORD, "dog");

        Text nodeId = new Text("dog");
        Node node = new Node("dog",
                             new AdjacencyNodes(ImmutableSet.of("dot", "cog", "log")),
                             0, // distance == 0 for begin node
                             ImmutableSet.of());

        reduceDriver.withInput(new Pair<>(nodeId, ImmutableList.of(
                                                            new AdjacencyNodes(ImmutableSet.of("dot")),
                                                            new AdjacencyNodes(ImmutableSet.of("cog", "log")))))
                    .withOutput(nodeId, node)
                    .runTest();
    }

}

package com.pebblepost.interview

import org.apache.spark.{SparkConf, SparkContext}
import scaldi.Module

/**
  * @author arevkov
  */
object JobLauncher extends Module {

  bind [Words] to new WordsYourImpl

  def main(args: Array[String]) {
    if (args.length != 3) {
      println("Usage: java JobLauncher <wordsListPath> <beginWord> <endWord>")
      System.exit(1)
    }
    val Array(wordsListPath, beginWord, endWord) = args

    val transformationLength: Int = runLocal(wordsListPath, beginWord, endWord)
    println(s"Transformation Length: $transformationLength")
  }

  def run(conf: SparkConf, wordsListPath: String, beginWord: String, endWord: String): Int = {
    val sc: SparkContext = new SparkContext(conf)

    try {
      val words = inject [Words]

      words.findShortestTransformation(sc, wordsListPath, beginWord, endWord)
    } finally {
      sc.stop()
    }
  }

  /**
    * Use it in test purposes
    */
  def runLocal(wordsListPath: String, beginWord: String, endWord: String): Int = {
    val conf: SparkConf = new SparkConf()
      .setAppName("words")
      .setMaster("local[*]") // run Spark locally with as many working threads as logical cores on local machine

    run(conf, wordsListPath, beginWord, endWord)
  }
}

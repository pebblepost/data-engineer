Words
=====
Given two words (beginWord and endWord), and a dictionary's word list, find the length of shortest transformation sequence from beginWord to endWord, such that:

Only one letter can be changed at a time.
Each transformed word must exist in the word list. Note that beginWord is not a transformed word.
For example,

Given:
beginWord = "hit"
endWord = "cog"
wordList = ["hot","dot","dog","lot","log","cog"]
As one shortest transformation is "hit" -> "hot" -> "dot" -> "dog" -> "cog",
return its length 5.

Note:
Return 0 if there is no such transformation sequence.
All words have the same length.
All words contain only lowercase alphabetic characters.
You may assume no duplicates in the word list.
You may assume beginWord and endWord are non-empty and are not the same.

The task must be implemented using Apache Hadoop API. WordCount.java and WordCountTest.java are provided as an example how to write and test Hadoop jobs.
You task is to write appropriate Hadoop jobs and implement the method int MainTest.words(String beginWord, String endWord, List<String> wordList).

## Hot to run tests
'''
mvn test
'''

## How to build
'''
mvn clean package
'''

## How to run
'''
java -cp words.jar com.pebblepost.interview.JobLauncher <wordsListPath> <beginWord> <endWord>
'''

## How to reduce the project down to test assignment level
1. delete all `impl` packages in both production and test source folders
2. then a candidate needs to implement `Words` trait properly

## Some considerations on using Docker for test purposes
Running docker container requires to download an image of about 1G size at first launcher.
Moreover there aren't any image in (https://hub.docker.com) with latest version of Spark.
At the same time Spark API allows to run a job in local environment.
So I considered it more appropriate to run jobs in local mode for test purposes rather
than bringing up a Docker image with Hadoop and Spark installed.

## Some thoughts on test assignment itself
If the test assignment is somehow related to Pebblepost bussines tasks,
it seems like a real overkill to run a single MapReduce or Spark job on a whole bunch of data
to just answer a query on what is a shortest transformation path between two particular words.
It rather looks like a task to some Graph DB.

On previous job we piloted a couple of graph solutions to process social network data and find
friends of friends to given user with low latency. Among those products were Neo4j and TitanDB(based on Cassandra)
Both DBs were rejected due to scalability issues and long time on adding entire social network graph to DB.

After all we wrote a Spark job whose only purposes was to create compact binary representation of graph(adjacency list)
from a big file on HDFS with social network data. After that a java program brought this file into memory
and was able to reply and query to graph with high throughput and low latency. The solution had two downfalls:
1. graph wasn't horizontally scaled a.k.a. partitioned to be distributed on multiple machines.
so it required a box with decent amount of RAM to bring it all to memory
2. graph didn't allow interactive mutations. the whole graph was rebuilt periodically.

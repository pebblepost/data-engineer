package com.pebblepost.interview.impl

import com.pebblepost.interview.impl.StringUtils.hammingDistance
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner


/**
  * @author arevkov
  */
@RunWith(classOf[JUnitRunner])
class StringUtilsSpec extends Specification {

  "Hamming Distance specification" >> {

    "trying to calc the distance between words of different length should throw IllegalArgumentException" >> {
      hammingDistance("dog", "summer") must throwA(new IllegalArgumentException("words should have the same length"))
    }

    "distance to the same string should be 0" >> {
      hammingDistance("dog", "dog") mustEqual 0
    }

    "distance between `flip` and `flop` is 1" >> {
      hammingDistance("flip", "flop") mustEqual 1
    }

    "distance between `this` and `that` is 2" >> {
      hammingDistance("this", "that") mustEqual 2
    }
  }
}

package com.pebblepost.interview

import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner

/**
  * @author arevkov
  */
@RunWith(classOf[JUnitRunner])
class WordsSpec extends Specification {
  // running multiple instances of local SparkContext fails
  sequential

  "Find Shortest Transformation specification" >> {

    "length of shortest transformation from `hit` to `cog` with words0.txt is 5" >> {
      JobLauncher.runLocal("data/words0.txt", "hit", "cog") mustEqual 5
    }

    "length of shortest transformation from `cet` to `ism` with words1.txt is 11" >> {
      JobLauncher.runLocal("data/words1.txt", "cet", "ism") mustEqual 11
    }

    "length of shortest transformation from `sand` to `acne` with words2.txt is 11" >> {
      JobLauncher.runLocal("data/words2.txt", "sand", "acne") mustEqual 11
    }
  }
}

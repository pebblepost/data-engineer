package com.pebblepost.interview

import org.apache.spark.SparkContext

/**
  * @author arevkov
  */
trait Words {

  /**
    * TODO: copy description from README
    *
    * @param sc            SparkContext
    * @param wordsListPath file with set of words that can participate in transformation
    * @param beginWord     word to be transformed
    * @param endWord       target word
    * @return number of words in transformation path from beginWord to endWord
    */
  def findShortestTransformation(sc: SparkContext, wordsListPath: String, beginWord: String, endWord: String): Int
}

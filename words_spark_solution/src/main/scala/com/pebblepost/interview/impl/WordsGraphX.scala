package com.pebblepost.interview.impl

import com.pebblepost.interview.Words
import org.apache.spark.SparkContext
import org.apache.spark.graphx.Graph
import org.apache.spark.graphx.lib.ShortestPaths
import org.apache.spark.rdd.RDD

/**
  * @author arevkov
  */
class WordsGraphX extends Words {

  /**
    * TODO: copy description from README
    *
    * @param sc            SparkContext
    * @param wordsListPath file with set of words that can participate in transformation
    * @param beginWord     word to be transformed
    * @param endWord       target word
    * @return number of words in transformation path from beginWord to endWord
    */
  override def findShortestTransformation(sc: SparkContext, wordsListPath: String, beginWord: String, endWord: String): Int = {
    val vertices: RDD[(String, Long)] = sc.textFile(wordsListPath)
      .union(sc.parallelize(Seq(beginWord))) // beginWord is not a transformed word
      .zipWithUniqueId() // assign unique long to each word in word list(is meant to be VertexID)

    // all pairs of words will represent the edges in graph with Hamming Distance as weights
    // then filter only those edges with weight equals to 1
    //
    // PS.: keeping in mind that cross product produces N*N result set
    // here we could make an optimization having Cartesian product only on words of the same length,
    // but by the conditions of tasks all words in wordsList already have same length
    val edges: RDD[(Long, Long)] = vertices.cartesian(vertices)
      .filter(edge => StringUtils.hammingDistance(edge._1._1, edge._2._1) == 1) // Only one letter can be changed at a time.
      .map(edge => edge._1._2 -> edge._2._2)

    val graph: Graph[Unit, Int] = Graph.fromEdgeTuples(edges, {})

    // get IDs of source and destination vertices in path
    val srdId: Long = getVertexIdByName(beginWord, vertices)
    val dstId: Long = getVertexIdByName(endWord, vertices)

    // unfortunately, Spark GraphX ShortestPath doesn't provide an API
    // to retrieve the path itself but only its length ((
    // (it can be a real stopper to use it in real tasks)
    val shortestPathLength: Option[Int] = ShortestPaths.run(graph, Seq(dstId))
      .vertices
      .filter({ case (vId, _) => vId == srdId })
      .first
      ._2
      .get(dstId)

    // by definition: the number of words in transformation path including beginWord and endWord
    // as all the edges in graph have weight equals to 1, so shortestPathLength corresponds to
    // the number of transformations(edges) and we just need to increment it to get the number of
    // vertices in the path
    val transformationLength = shortestPathLength.get + 1

    transformationLength
  }

  def getVertexIdByName(wordA: String, verteces: RDD[(String, Long)]): Long = {
    verteces.filter(_._1 == wordA).take(1).head._2
  }
}

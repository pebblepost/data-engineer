package com.pebblepost.interview.impl

/**
  * @author arevkov
  */
object StringUtils {

  def hammingDistance(wordA: String, wordB: String): Int = {
    if (wordA.length != wordB.length) {
      throw new IllegalArgumentException("words should have the same length")
    }
    wordA.zip(wordB).map(p => if (p._1 == p._2) 0 else 1).sum
  }
}

package com.pebblepost.interview;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.stream.StreamSupport;

/**
 * Sample Hadoop word-count job
 */
public class WordLadder {

    public static class WMapper extends Mapper<Object, Text, Text, IntWritable> {

        private static final IntWritable ONE = new IntWritable(1);

        @Override
        protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            context.write(value, ONE);
        }

    }

    public static class WReducer extends Reducer<Text, IntWritable, Text, IntWritable> {

        @Override
        protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
            final int sum = StreamSupport.stream(values.spliterator(), false).mapToInt(IntWritable::get).sum();
            context.write(key, new IntWritable(sum));
        }

    }

}

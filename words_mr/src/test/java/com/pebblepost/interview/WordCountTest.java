package com.pebblepost.interview;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class WordCountTest extends AbstractMiniDFSClusterTest {

    private static final String IN_DIR = "testing/words/input";
    private static final String OUT_DIR = "testing/words/output";
    private static final String DATA_FILE = "sample.txt";

    /**
     * Test of sample Hadoop word-count job
     */
    @Test
    public void testWordCount() throws Throwable {
        Path inDir = new Path(IN_DIR);
        Path outDir = new Path(OUT_DIR);

        fs.delete(inDir, true);
        fs.delete(outDir, true);

        writeHDFSContent(fs, inDir, DATA_FILE, readFromResources("words1.txt"));

        Job job = Job.getInstance(conf, WordCount.class.getName());
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        job.setMapperClass(WordCount.WordsMapper.class);
        job.setReducerClass(WordCount.WordsReducer.class);
        FileInputFormat.addInputPath(job, inDir);
        FileOutputFormat.setOutputPath(job, outDir);
        job.waitForCompletion(true);
        assertTrue(job.isSuccessful());

        List<String[]> results = getJobResults(fs, outDir);

        assertEquals(599, results.size());
        results.forEach(r -> {
            System.out.println(Arrays.toString(r));
            assertEquals("1", r[1]);
        });
    }

}
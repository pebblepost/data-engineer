package com.pebblepost.interview;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.HdfsConfiguration;
import org.apache.hadoop.hdfs.MiniDFSCluster;
import org.apache.hadoop.test.PathUtils;
import org.junit.After;
import org.junit.Before;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertNull;

public abstract class AbstractMiniDFSClusterTest {

    protected static final String CLUSTER_1 = "cluster1";

    protected File testDataPath;
    protected Configuration conf;
    protected MiniDFSCluster cluster;

    protected FileSystem fs;

    @Before
    public void setUp() throws Exception {
        testDataPath = new File(PathUtils.getTestDir(getClass()), "miniclusters");
        System.clearProperty(MiniDFSCluster.PROP_TEST_BUILD_DATA);
        conf = new HdfsConfiguration();
        File testDataCluster1 = new File(testDataPath, CLUSTER_1);
        String c1Path = testDataCluster1.getAbsolutePath();
        conf.set(MiniDFSCluster.HDFS_MINIDFS_BASEDIR, c1Path);
        cluster = new MiniDFSCluster.Builder(conf).build();
        fs = FileSystem.get(conf);
    }

    @After
    public void tearDown() throws Exception {

    }

    protected void writeHDFSContent(FileSystem fs, Path dir, String fileName, List<String> content) throws IOException {
        Path newFilePath = new Path(dir, fileName);
        FSDataOutputStream out = fs.create(newFilePath);
        for (String line : content) {
            out.writeBytes(line + "\n");
        }
        out.close();
    }

    protected List<String[]> getJobResults(FileSystem fs, Path outDir) throws Exception {
        List<String[]> results = new ArrayList<>();
        FileStatus[] fileStatus = fs.listStatus(outDir);
        for (FileStatus file : fileStatus) {
            String name = file.getPath().getName();
            if (name.contains("part-r-00000")) {
                Path filePath = new Path(outDir + "/" + name);
                BufferedReader reader = new BufferedReader(new InputStreamReader(fs.open(filePath)));
                String line;
                while ((line = reader.readLine()) != null) {
                    results.add(line.split("\\s+"));
                }
                assertNull(reader.readLine());
                reader.close();
            }
        }
        return results;
    }

    protected static List<String> readFromResources(String name) {
        try {
            return new BufferedReader(new InputStreamReader(AbstractMiniDFSClusterTest.class.getResourceAsStream("/" + name))).lines().collect(Collectors.toList());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}

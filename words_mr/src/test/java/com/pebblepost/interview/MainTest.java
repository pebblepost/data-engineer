package com.pebblepost.interview;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class MainTest extends AbstractMiniDFSClusterTest {

    /**
     * 0) Read WordCount.java and WordCountTest.java to understand how to use testing environment
     * 1) Save the wordList to the HDFS
     * 2) Run your hadoop jobs
     * 3) Read the result from the HDFS
     * 4) Return the result
     *
     * @param beginWord the starting word e.g. "hit"
     * @param endWord   the ending word e.g. "cog"
     * @param wordList  the dictionary of all allowed words e.g. ["hot","dot","dog","lot","log","cog"]
     * @return the length of one shortest transformation e.g. "hit" -> "hot" -> "dot" -> "dog" -> "cog"
     */
    int words(String beginWord, String endWord, List<String> wordList) throws Throwable {
        // IMPLEMENT THIS
        return 0;
    }

    @Test
    public void testWords0() throws Throwable {
        //As one shortest transformation is "hit" -> "hot" -> "dot" -> "dog" -> "cog",
        assertEquals(5, words("hit", "cog", Arrays.asList("hot", "dot", "dog", "hit", "lot", "log", "cog")));
    }

    @Test
    public void testWords1() throws Throwable {
        assertEquals(11, words("cet", "ism", readFromResources("words1.txt")));
    }

    @Test
    public void testWords2() throws Throwable {
        assertEquals(11, words("sand", "acne", readFromResources("words2.txt")));
    }

}
